import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/', name: 'common', component: () => import('@/views/fore/Common.vue'), redirect: '/home',
    children: [
      { path: '/home', name: 'home', component: () => import('@/views/fore/Home.vue') },
      { path: '/animalCards', name: 'animalCards', component: () => import('@/views/fore/AnimalCards.vue') },
      { path: '/notice', name: 'notice', component: () => import('@/views/fore/Notice.vue') },
      { path: '/scenicSpotAndZoo', name: 'scenicSpotAndZoo', component: () => import('@/views/fore/ScenicSpotAndZoo.vue') },
      { path: '/dataDownload', name: 'dataDownload', component: () => import('@/views/fore/DataDownload.vue') },
    ]
  },
  {
    path: '/manager', name: 'manager', component: () => import('@/views/back/Manager.vue'), redirect: '/backHome',
    children: [
      //后台数据管理
      { path: '/backHome', name: 'backHome', component: () => import('@/views/back/HomeView.vue') },
      { path: '/user', name: 'user', component: () => import('@/views/back/UserView.vue') },
      { path: '/file', name: 'file', component: () => import('@/views/back/FileView.vue') },
      { path: '/permissionMenu', name: 'permissionMenu', component: () => import('@/views/back/PermissionMenuView.vue') },
      { path: '/role', name: 'role', component: () => import('@/views/back/RoleView.vue') },
      { path: '/personal', name: 'personal', component: () => import('@/views/back/PersonalView.vue') },
      { path: '/bresetPass', name: 'bresetPass', component: () => import('@/views/back/BresetPass.vue') },
      { path: '/bforgetPass', name: 'bforgetPass', component: () => import('@/views/back/BforgetPass.vue') },
      //前台数据管理
      { path: '/bswiper', name: 'bswiper', component: () => import('@/views/back/Bswiper.vue') },
      { path: '/banimal', name: 'banimal', component: () => import('@/views/back/Banimal.vue') },
      { path: '/bnotice', name: 'bnotice', component: () => import('@/views/back/Bnotice.vue') },
      { path: '/bwebsite', name: 'bwebsite', component: () => import('@/views/back/Bwebsite.vue') },
      { path: '/bnews', name: 'bnews', component: () => import('@/views/back/Bnews.vue') },
      { path: '/bscenic', name: 'bscenic', component: () => import('@/views/back/Bscenic.vue') },
      { path: '/bapply', name: 'bapply', component: () => import('@/views/back/Bapply.vue') },

    ]
  },
  { path: '/login', name: 'login', component: () => import('@/views/back/LoginView.vue') },
  { path: '/register', name: 'register', component: () => import('@/views/back/RegisterView.vue') }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//导航守卫
router.beforeEach((to,from,next) => {
  if(to.path === '/backHome' && from.path !== '/login'){
    const token = JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')).token : null;
    if(token){
      next();
    }else{
      next("/login");
    }
  }else{
    next();
  }
})
export default router
