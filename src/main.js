import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/global.css'
import axios from 'axios'
import VueClipboard from 'vue-clipboard2'

Vue.use(VueClipboard)

Vue.config.productionTip = false

const localIp = "localhost";
const cloudIp = "120.24.92.155";
const tempIp = cloudIp;

//全局配置 axios 的请求根路径
// axios.defaults.baseURL = 'http://localhost:9191'
axios.defaults.baseURL = 'http://' + tempIp + ':9191'
//设置验证白名单
const whiteUrls = ['/login', '/register','/news/**','/swiper/**']
//把 axios 挂载到 Vue.prototype 上，供每个 .vue 组件实例直接使用
Vue.prototype.request = axios
//在以后使用 axios 时，可以直接调用 this.request.xxx
Vue.prototype.tempIp = tempIp

//request拦截器
//在请求前做处理
Vue.prototype.request.interceptors.request.use(req => {
  req.headers['Content-Type'] = 'application/json;charset=utf-8';
  let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
  if (!whiteUrls.includes(req.url)) {
    if (!user) {
      // ElementUI.Message({
      //   message: '当前登录已失效，请重新登录！',
      //   type: 'error'
      // });
      // router.push('/login')
    } else {
      req.headers['token'] = user.token;  // 设置请求头
    }
  }
  return req;
}, error => {
  return Promise.reject(error)
});

//对 axios 返回的数据进行封装，得到原先 res.data 的数据
// response 拦截器
// 可以在接口响应后统一处理结果
Vue.prototype.request.interceptors.response.use(res => {
  // 验证token是否失效
  if (res.data.code == '000-402') {
    ElementUI.Message({
      message: '当前token已过期，请退出重新登录！',
      type: 'error'
    });
    localStorage.removeItem('user');
  }
  return res.data;
});

Vue.use(ElementUI, { size: 'mini' })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

export default tempIp;
